package aoc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Objects;

public class AOC {

    public static List<String> lines(String path) throws IOException {
        try (var reader = new BufferedReader(new InputStreamReader(Objects.requireNonNull(AOC.class.getResourceAsStream(path))))) {
            return reader.lines().toList();
        }
    }
}
