package aoc;

import java.util.ArrayList;
import java.util.Comparator;

public class Day1 {
    public static void main(String[] args) throws Exception {
        var lines = AOC.lines("/aoc/Day1.txt");
        ArrayList<Integer> elves = new ArrayList<>();
        var elf = 0;
        for (var line : lines) {
            if (line.isEmpty()) {
                elves.add(elf);
                elf = 0;
            } else {
                elf += Integer.parseInt(line);
            }
        }
        System.out.println("Part 1: " + elves.stream().mapToInt(Integer::intValue).max().orElse(0));
        System.out.println("Part 2: " + elves.stream().sorted(Comparator.reverseOrder()).limit(3).mapToInt(Integer::intValue).sum());
    }
}
