package aoc;

public class Day2 {
    public enum Play {
        Rock(1), Paper(2), Scissors(3);

        public final int score;

        Play(int score) {
            this.score = score;
        }

        public Play defeats() {
            return switch (this) {
                case Rock -> Play.Scissors;
                case Paper -> Play.Rock;
                case Scissors -> Play.Paper;
            };
        }

        public Play losesTo() {
            return (switch (this) {
                case Rock -> Play.Paper;
                case Paper -> Play.Scissors;
                case Scissors -> Play.Rock;
            });
        }
    }
    public enum Result {
        Win(6), Tie(3), Lose(0);

        public final int score;

        Result(int score) {
            this.score = score;
        }

        public Play play(Play play) {
            return switch (this) {
                case Win -> play.losesTo();
                case Tie -> play;
                case Lose -> play.defeats();
            };
        }
    }

    public static Play mapPlay(char play) {
        if (play == 'A' || play == 'X') return Play.Rock;
        if (play == 'B' || play == 'Y') return Play.Paper;
        if (play == 'C' || play == 'Z') return Play.Scissors;
        throw new IllegalArgumentException(Character.toString(play));
    }

    public static Result mapResult(char result) {
        if (result == 'X') return Result.Lose;
        if (result == 'Y') return Result.Tie;
        if (result == 'Z') return Result.Win;
        throw new IllegalArgumentException(Character.toString(result));
    }

    public static void main(String[] args) throws Exception {
        var lines = AOC.lines("/aoc/Day2.txt");
        var score = 0;
        for (var line : lines) {
            var player1 = mapPlay(line.charAt(0));
            var player2 = mapPlay(line.charAt(2));
            score += player2.score;
            if (player1 == player2) {
                score += 3;
            }
            if (player2 == player1.losesTo()) {
                score += 6;
            }
        }
        System.out.println("Part 1: " + score);
        score = 0;
        for (var line : lines) {
            var player1 = mapPlay(line.charAt(0));
            var result = mapResult(line.charAt(2));
            score += result.score + result.play(player1).score;
        }
        System.out.println("Part 2: " + score);
    }
}
