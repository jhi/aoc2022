package aoc;

import java.util.Set;
import java.util.stream.Collectors;

public class Day3 {
    public static void main(String[] args) throws Exception {
        var lines = AOC.lines("/aoc/Day3.txt");
        var total = 0;
        for (var line : lines) {
            var compartmentSize = line.length() / 2;
            var compartment1 = line.substring(0, compartmentSize);
            var compartment2 = line.substring(compartmentSize);
            boolean found = false;
            for (var item1 : compartment1.toCharArray()) {
                for (var item2 : compartment2.toCharArray()) {
                    if (item1 == item2) {
                        found = true;
                        if (item1 <= 'Z') {
                            total += item1 - 'A' + 1 + 26;
                        } else {
                            total += item1 - 'a' + 1;
                        }
                        break;
                    }
                }
                if (found) {
                    break;
                }
            }
        }
        System.out.println("Part 1: " + total);
        total = 0;
        Set<Integer> set1 = null;
        Set<Integer> set2 = null;
        for (var line : lines) {
            if (set1 == null) {
                set1 = line.chars().boxed().collect(Collectors.toSet());
                continue;
            }
            if (set2 == null) {
                set2 = line.chars().boxed().collect(Collectors.toSet());
                continue;
            }
            Set<Integer> set3 = line.chars().boxed().collect(Collectors.toSet());
            set3.retainAll(set2);
            set3.retainAll(set1);
            var item = set3.iterator().next();
            if (item <= 'Z') {
                total += item - 'A' + 1 + 26;
            } else {
                total += item - 'a' + 1;
            }
            set1 = null;
            set2 = null;
        }
        System.out.println("Part 2: " + total);
    }
}
