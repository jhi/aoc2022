package aoc;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day4 {
    public record Range(int start, int end) {

        public boolean contains(Range range) {
            return start <= range.start && end >= range.end;
        }

        public boolean contains(int value) {
            return value >= start && value <= end;
        }

        public boolean overlaps(Range range) {
            return range.contains(start) || contains(range.start) || range.contains(end) || contains(range.end);
        }
    }

    public static void main(String[] args) throws Exception {
        var lines = AOC.lines("/aoc/Day4.txt");
        var part1 = 0;
        var part2 = 0;
        Pattern pattern = Pattern.compile("(\\d+)-(\\d+),(\\d+)-(\\d+)");
        for (var line : lines) {
            Matcher matcher = pattern.matcher(line);
            if (matcher.matches()) {
                var range1 = new Range(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2)));
                var range2 = new Range(Integer.parseInt(matcher.group(3)), Integer.parseInt(matcher.group(4)));
                if (range1.contains(range2) || range2.contains(range1)) {
                    part1++;
                }
                if (range1.overlaps(range2)) {
                    System.out.println("Y " + line);
                    part2++;
                } else {
                    System.out.println("N " + line);
                }
            }
        }
        System.out.println("Part 1: " + part1);
        System.out.println("Part 2: " + part2);
    }
}
