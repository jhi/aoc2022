package aoc;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class Day5 {
    public record Move(int count, int from, int to) {}

    public static void main(String[] args) throws Exception {
        var lines = AOC.lines("/aoc/Day5.txt");
        var stacks1 = new ArrayList<ArrayList<Character>>();
        var stacks2 = new ArrayList<ArrayList<Character>>();
        var moves = new ArrayList<Move>();
        var stackPattern = Pattern.compile("\\s?\\s\\s\\s|\\[[A-Z]]");
        var movePattern = Pattern.compile("move (\\d+) from (\\d+) to (\\d+)");
        var parseStacks = true;
        for (var line : lines) {
            if (parseStacks) {
                if (line.isEmpty()) {
                    parseStacks = false;
                    continue;
                }
                var stackIndex = 0;
                var matcher = stackPattern.matcher(line);
                while (matcher.find()) {
                    if (stackIndex >= stacks1.size()) {
                        stacks1.add(new ArrayList<>());
                        stacks2.add(new ArrayList<>());
                    }
                    var match = matcher.group();
                    if (match.startsWith("[") || match.startsWith(" [")) {
                        stacks1.get(stackIndex).add(match.charAt(1));
                        stacks2.get(stackIndex).add(match.charAt(1));
                    }
                    stackIndex++;
                }
            } else {
                var matcher = movePattern.matcher(line);
                if (matcher.matches()) {
                    moves.add(new Move(
                            Integer.parseInt(matcher.group(1)),
                            Integer.parseInt(matcher.group(2)),
                            Integer.parseInt(matcher.group(3))
                    ));
                }
            }
        }
        for (var move : moves) {
            // Part 1
            for (var i = 0; i < move.count; i++) {
                var item = stacks1.get(move.from - 1).remove(0);
                stacks1.get(move.to - 1).add(0, item);
            }
            // Part 2
            for (var i = move.count - 1; i >= 0; i--) {
                var item = stacks2.get(move.from - 1).remove(i);
                stacks2.get(move.to - 1).add(0, item);
            }
        }
        var part1 = new StringBuilder();
        for (var stack : stacks1) {
            part1.append(stack.get(0));
        }
        var part2 = new StringBuilder();
        for (var stack : stacks2) {
            part2.append(stack.get(0));
        }
        System.out.println("Part 1: " + part1);
        System.out.println("Part 2: " + part2);
    }
}
