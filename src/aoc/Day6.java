package aoc;

public class Day6 {
    public static int find(String line, int length) {
        int count = 0;
        for (var i = 0; i < line.length() - length + count; i++) {
            char c = line.charAt(i);
            boolean reset = false;
            for (var j = 1; j < length - count; j++) {
                if (c == line.charAt(i + j)) {
                    reset = true;
                    break;
                }
            }
            if (reset) {
                count = 0;
            } else if (++count == length) {
                return i + 1;
            }
        }
        return -1;
    }

    public static void main(String[] args) throws Exception {
        var line = AOC.lines("/aoc/Day6.txt").get(0);
        System.out.println("Part 1: " + find(line, 4));
        System.out.println("Part 2: " + find(line, 14));
    }
}
